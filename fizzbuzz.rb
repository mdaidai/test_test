def fizzbuzz
  return 'FizzBuzz' if self % 10 == 0
  return 'Buzz' if self % 7 == 0
  return 'Fizz' if self % 5 == 0
  self
end
